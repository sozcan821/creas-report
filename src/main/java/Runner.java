import com.creas.export.CrsXls;
import com.creas.export.CrsXlsx;
import com.creas.export.CrsCsv;

public class Runner {

    public static void main(String[] args)
    {
        CrsXls excel = new CrsXls();
        try
        {
            excel.createWorkbook("C:/work/temp/Excel.xls");
            excel.setActiveSheet("asd");
            excel.setActiveSheet("def");
            excel.setActiveSheet("asd");
            excel.setCellValue(0,0,"Test");
            excel.setCellValue(0,1,"A");
            excel.setCellValue(1,0,"One");
            excel.setCellValue(1,1,"(_)");
            excel.setActiveSheet("def");
            excel.setCellValue(0,0,"Test");
            excel.deleteSheet("def");
            excel.setActiveSheet("asd");
            excel.deleteCellValue(1,0);
            excel.writeExcel();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}
