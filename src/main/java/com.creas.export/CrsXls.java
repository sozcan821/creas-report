package com.creas.export;

import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFPalette;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 *  Class for creating or importing .xls files
 */
public class CrsXls
{
    private HSSFWorkbook activeWorkbook;
    private HSSFSheet activeSheet;
    private String workbookPath;

    private static final Logger logger
            = LoggerFactory.getLogger(CrsXls.class);

    /**
     * Sets the path of the active workbook to the user's Temp folder. (C:/Users/user/AppData/Local/Temp)
     *
     * @return this CrsXls object
     */
    public CrsXls createWorkbook()
    {
        return createWorkbook("");
    }

    /**
     * Sets the path of the active workbook to the specified path.
     * If a file already exist on the given path, it is overwritten.
     *
     * @param path filepath of the file to be created
     * @return this CrsXls object
     */
    public CrsXls createWorkbook(String path)
    {
        workbookPath = path;
        activeWorkbook = new HSSFWorkbook();
        logger.info("Created new workbook");
        return this;
    }

    /**
     * Imports an existing .xls file on the specified path.
     * Modifications are appended to the already existing file.
     *
     * @param importPath filepath of the file to be imported
     * @return this CrsXls object
     * @throws IOException
     */
    public CrsXls importWorkbook(String importPath) throws IOException {
        workbookPath = importPath;
        FileInputStream fis = new FileInputStream(importPath);
        activeWorkbook = (HSSFWorkbook) WorkbookFactory.create(fis);
        fis.close();
        logger.info("Imported existing workbook");
        return this;
    }

    /**
     * Creates a sheet with the given name if it doesn't exist,
     * then sets the sheet with the given name as active.
     *
     * @param sheetName name of the sheet
     */
    public void setActiveSheet(String sheetName)
    {
        activeSheet = activeWorkbook.getSheet(sheetName);
        if (activeSheet == null)
        {
            activeSheet = activeWorkbook.createSheet(sheetName);
            logger.info("Created new sheet: " + sheetName);
        }
        else
            logger.info("Got existing sheet: " + sheetName);
    }

    /**
     * Sets a String value to the cell at the given coordinates.
     *
     * @param rowIndex index number of the row (0 corresponds to row "1")
     * @param columnIndex index number of the column (0 corresponds to column "A")
     * @param value String value to be set in the cell
     */
    public void setCellValue(int rowIndex, int columnIndex, String value)
    {
        Row row = activeSheet.getRow(rowIndex);
        if (row == null)
        {
            row = activeSheet.createRow(rowIndex);
            logger.info("Created new row: " + rowIndex);
        }
        else
            logger.info("Got existing row: " + rowIndex);

        Cell cell = row.getCell(columnIndex);
        if (cell == null)
        {
            cell = row.createCell(columnIndex);
            logger.info("Created new cell: " + columnIndex);
        }
        else
            logger.info("Got existing cell: " + columnIndex);

        cell.setCellValue(value);
        logger.info("Set cell value: " + value);
    }

    /**
     * Deletes the sheet with the given name.
     *
     * @param sheetName name of the sheet to be deleted
     */
    public void deleteSheet(String sheetName)
    {
        HSSFSheet sheet = activeWorkbook.getSheet(sheetName);
        if (sheet != null)
        {
            int index = activeWorkbook.getSheetIndex(sheet);
            if (index == -1)
                logger.info("Sheet not found: " + sheetName);
            else
            {
                activeWorkbook.removeSheetAt(index);
                logger.info("Deleted sheet: " + sheetName);
            }
        }
    }

    /**
     * Sets the cell at the given coordinates as blank.
     * This does not reset cell styling.
     *
     * @param rowIndex index number of the row (0 corresponds to row "1")
     * @param columnIndex index number of the column (0 corresponds to column "A")
     */
    public void deleteCellValue(int rowIndex, int columnIndex)
    {
        Row row = activeSheet.getRow(rowIndex);
        if (row == null)
        {
            row = activeSheet.createRow(rowIndex);
            logger.info("Created new row: " + rowIndex);
        }
        else
            logger.info("Got existing row: " + rowIndex);

        Cell cell = row.getCell(columnIndex);
        if (cell == null)
        {
            cell = row.createCell(columnIndex);
            logger.info("Created new cell: " + columnIndex);
        }
        else
            logger.info("Got existing cell: " + columnIndex);


        cell.setCellValue((String) null);
        logger.info("Deleted cell value");
    }

    /**
     * Sets the background color of the cells inside the given coordinates.
     *
     * @param rowIndex1 first row index
     * @param rowIndex2 last row index, inclusive
     * @param columnIndex1 first column index
     * @param columnIndex2 last column index, inclusive
     * @param r red value of the color to be used
     * @param g green value of the color to be used
     * @param b blue value of the color to be used
     */
    public void setCellColor(int rowIndex1, int rowIndex2, int columnIndex1, int columnIndex2, int r, int g, int b)
    {
        HSSFPalette palette = activeWorkbook.getCustomPalette();
        palette.setColorAtIndex(HSSFColor.HSSFColorPredefined.WHITE.getIndex(),
                (byte) r,
                (byte) g,
                (byte) b
        );

        HSSFCellStyle cellStyle = activeWorkbook.createCellStyle();
        cellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        cellStyle.setFillForegroundColor(HSSFColor.HSSFColorPredefined.WHITE.getIndex());

        for (int i = rowIndex1; i <= rowIndex2; i++)
        {
            Row row = activeSheet.getRow(i);
            if (row == null)
            {
                row = activeSheet.createRow(i);
                logger.info("Created new row: " + i);
            }
            else
                logger.info("Got existing row: " + i);

            for (int j = columnIndex1; j <= columnIndex2; j++)
            {
                Cell cell = row.getCell(j);
                if (cell == null)
                {
                    cell = row.createCell(j);
                    logger.info("Created new cell: " + j);
                }
                else
                    logger.info("Got existing cell: " + j);

                cell.setCellStyle(cellStyle);
            }
        }
    }

    /**
     * Writes the current version of the workbook to the path set in creation/import.
     *
     * @throws IOException
     */
    public void writeExcel() throws IOException {
        File directory;
        if (workbookPath.equals(""))
        {
            directory = File.createTempFile("excel-", ".xls");
            workbookPath = directory.getPath();
        }
        else
            directory = new File(workbookPath.substring(0,workbookPath.lastIndexOf("/")));

        if (!directory.exists())
            directory.mkdir();

        if (!directory.exists()) {
            logger.info("Export folder could not be created: " + workbookPath);
            return;
        }

        FileOutputStream fos = new FileOutputStream(workbookPath);
        activeWorkbook.write(fos);
        fos.close();
        logger.info("File write complete");
    }
}
