package com.creas.export;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 *  Class for creating or importing .csv files
 */
public class CrsCsv
{
    private FileWriter fileWriter;
    private CSVPrinter csvPrinter;

    private static final Logger logger
            = LoggerFactory.getLogger(CrsCsv.class);

    /**
     * Sets the path of the active workbook to the user's Temp folder. (C:/Users/user/AppData/Local/Temp)
     *
     * @return this CrsCsv object
     * @throws IOException
     */
    public CrsCsv createWorkbook() throws IOException
    {
        return createWorkbook("");
    }

    /**
     * Sets the path of the active workbook to the specified path.
     * If a file already exist on the given path, it is overwritten.
     *
     * @param path filepath of the file to be created
     * @return this CrsCsv object
     * @throws IOException
     */
    public CrsCsv createWorkbook(String path) throws IOException
    {
        if (path.equals(""))
            fileWriter = new FileWriter(File.createTempFile("excel-",".csv"), false);
        else
            fileWriter = new FileWriter(path, false);
        csvPrinter = new CSVPrinter(fileWriter, CSVFormat.DEFAULT);
        logger.info("Created new workbook");
        return this;
    }

    /**
     * Imports an existing .xlsx file on the specified path.
     * Modifications are appended to the already existing file.
     *
     * @param importPath filepath of the file to be imported
     * @return this CrsXls object
     * @throws IOException
     */
    public CrsCsv importWorkbook(String importPath) throws IOException
    {
        fileWriter = new FileWriter(importPath, true);
        csvPrinter = new CSVPrinter(fileWriter, CSVFormat.DEFAULT);
        logger.info("Created new workbook");
        return this;
    }

    /**
     * Adds the given String arguments as a row.
     * Each argument is written to a separate column.
     *
     * @param args String args to be written
     * @throws IOException
     */
    public void addRow(String... args) throws IOException
    {
        StringBuilder valueBuilder = new StringBuilder();
        for (String arg : args)
        {
            valueBuilder.append(arg).append(";");
        }
        String value = valueBuilder.toString();
        csvPrinter.printRecord(value);
        logger.info("Added row of values: " + value);
    }

    /**
     * Writes the current version of the workbook to the path set in creation/import.
     *
     * @throws IOException
     */
    public void writeExcel() throws IOException {
        csvPrinter.flush();
        csvPrinter.close();
        logger.info("File write complete");
    }
}
